package ca.tp1.adt;

/**
 * Classe pour g�rer les exceptions li�es � une position dans un intervalle
 */
@SuppressWarnings("serial")
public class PositionException extends Exception{
	
	/**
	 * Constructeur sans argument
	 */	
	public PositionException() {
        super();
    }
	
	/**
	 * Permet d'initialiser le message
	 * @param message Message � afficher
	 */
	public PositionException(String message) {
        super(message);
    }

}
