package ca.tp1.adt.impl;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import ca.tp1.adt.GroupeTda;
import ca.tp1.adt.PositionException;

/**
 * GroupeImpl : Classe pour impl�menter l�interface 
 *              GroupeImpl<T> en utilisant la classe ArrayList<T>.
 */

public class GroupeImpl<T> implements GroupeTda<T> {

	List<T> liste;

	/**
	 * Constructeur par d�faut.
	 */

	public GroupeImpl() {

		liste = new ArrayList<T>();
	}

	private boolean estNull(T element) {

		boolean reponse = false;

		if (element == null) {

			reponse = true;
		}
		return reponse;
	}

	@Override
	public boolean ajouterDebut(T element) {

		boolean reponse = false;

		if (!estNull(element) && !estElement(element)) {

			liste.add(0, element);
			reponse = true;

		} else {

			reponse = false;
		}
		return reponse;
	}

	@Override
	public boolean ajouterFin(T element) {
		boolean reponse = false;

		if (!estNull(element) && !estElement(element)) {

			liste.add(element);
			reponse = true;

		} else {

			reponse = false;
		}
		return reponse;
	}

	@Override
	public boolean ajouter(int indice, T element) throws PositionException {

		boolean reponse = false;

		if (indice < 0 || indice > liste.size()) {

			throw new PositionException("l'indice est dans l'intervalle non valide");

		} else if (!estNull(element) && !estElement(element)) {

			liste.set(indice, element);

			reponse = true;

		}

		return reponse;
	}

	@Override
	public List<T> ajouterDebut(GroupeTda<T> groupe) {

		List<T> groupeExisteDeja = new ArrayList<T>();

		if (groupe != null && !groupe.estVide()) {

			T objtTemp;
			Iterator<T> itt = groupe.iterateur();

			while (itt.hasNext()) {

				objtTemp = itt.next();

				if ((!ajouterDebut(objtTemp))) {

					groupeExisteDeja.add(objtTemp);

				}
			}
		}

		return groupeExisteDeja;
	}

	@Override
	public List<T> ajouterFin(GroupeTda<T> groupe) {

		List<T> groupeExisteDeja = new ArrayList<T>();

		if (groupe != null && !groupe.estVide()) {

			T objtTemp;
			Iterator<T> itt = groupe.iterateur();

			while (itt.hasNext()) {

				objtTemp = itt.next();

				if ((!ajouterFin(objtTemp))) {

					groupeExisteDeja.add(objtTemp);

				}
			}
		}

		return groupeExisteDeja;
	}

	@Override
	public List<T> comparer(GroupeTda<T> groupe) {

		List<T> groupeInexistent = new ArrayList<T>();

		if (groupe != null && !groupe.estVide()) {

			T objtTemp;
			Iterator<T> itt = groupe.iterateur();

			while (itt.hasNext()) {

				objtTemp = itt.next();

				if ((!estElement(objtTemp))) {

					groupeInexistent.add(objtTemp);

				}
			}
		}

		return groupeInexistent;
	}

	@Override
	public boolean estElement(T element) {

		boolean reponse = false;

		if (liste.contains(element)) {

			reponse = true;

		} else {

			reponse = false;
		}

		return reponse;
	}

	@Override
	public int nbElements() {

		return liste.size();
	}

	@Override
	public boolean supprimer(T element) {

		boolean reponse = false;

		if (estElement(element)) {

			liste.remove(element);
			reponse = true;

		} else {

			reponse = false;
		}
		return reponse;
	}

	@Override
	public List<T> supprimer(GroupeTda<T> groupe) {

		List<T> groupeNonSuprimes = new ArrayList<T>();

		if (groupe != null && !groupe.estVide()) {

			T objtTemp;
			Iterator<T> itt = groupe.iterateur();

			while (itt.hasNext()) {

				objtTemp = itt.next();

				if ((!supprimer(objtTemp))) {

					groupeNonSuprimes.add(objtTemp);

				}
			}
		}

		return groupeNonSuprimes;
	}

	@Override
	public boolean remplacer(T elementARemplacer, T nouveauElement) {

		boolean reponse = false;

		if (!estNull(nouveauElement) && !estElement(nouveauElement)) {

			for (int i = 0; i < liste.size(); i++) {

				if (liste.get(i).equals(elementARemplacer)) {

					liste.set(i, nouveauElement);

					reponse = true;
				}
			}

		}

		return reponse;
	}


	@Override
	public boolean estVide() {

		return liste.isEmpty();
	}

	@Override
	public void vider() {

		liste.clear();
	}

	@Override
	public Iterator<T> iterateur() {

		return liste.iterator();
	}
}
