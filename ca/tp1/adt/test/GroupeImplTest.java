package ca.tp1.adt.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.tp1.adt.GroupeTda;
import ca.tp1.adt.impl.GroupeImpl;
import ca.tp1.adt.PositionException;

/**
 * GroupeImplTest : Classe pour tester toutes les m�thodes 
 *                  de la classe GroupeImpl
 */

public class GroupeImplTest {

	GroupeTda<Membre> unGroupe;

	Membre membre01;
	Membre membre02;
	Membre membre03;
	Membre membre04;

	@Before
	public void setUp() throws Exception {

		unGroupe = new GroupeImpl<Membre>();
		

		membre01 = new Membre("AA01", "Abraham", "Alex");
		membre02 = new Membre("MM02", "Matte", "Martin");
		membre03 = new Membre("SM03", "Silve", "Maxim");
		membre04 = new Membre("PE04", "Pointe", "Eric");
	}

	@After
	public void tearDown() throws Exception {

		unGroupe = null;

		membre01 = null;
		membre02 = null;
		membre03 = null;
		membre04 = null;
	}
	
	public Membre [] parcourirGroupe (){
		
		Iterator<Membre> itt = unGroupe.iterateur();
		Membre [] tab = new Membre[unGroupe.nbElements()]; 
		int i = 0; 
		
		while(itt.hasNext()){
			tab [i] = itt.next();
			i++; 
		}
		
		return tab; 
	}
	

	// Teste la methode ajouterDebut() Scenario 01

	@Test
	public void testAjouterDebutScenario01() {

		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);
		unGroupe.ajouterDebut(membre03);
		
		assertEquals(3, unGroupe.nbElements());
		
	// Scenario 01 : l'�l�ment n'est pas null et il n'existe pas dans le groupe courant
		
		unGroupe.ajouterDebut(membre04);
		
		Membre [] tab = parcourirGroupe (); 
		
		//La position du premier �l�ment du groupe est occupee pour le dernier element ajut�
		
		assertEquals(tab[0], membre04);
		assertEquals(tab[1], membre03);
		assertEquals(tab[2], membre02);
		assertEquals(tab[3], membre01);
		
		assertEquals(4, unGroupe.nbElements());
		
	}

	// Teste la methode ajouterDebut() Scenario 02

	@Test
	public void testAjouterDebutScenario02() {
		
		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);
		unGroupe.ajouterDebut(membre03);	
		
	// Scenario 02 : l'�l�ment n'est pas null MAIS il existe deja dans le groupe courant
		
		assertEquals(3, unGroupe.nbElements());
		
		unGroupe.ajouterDebut(membre01);
		
		Membre [] tab = parcourirGroupe (); 
		
		assertEquals(3, unGroupe.nbElements());
		
		//La position du premier �l�ment du groupe est occupee pour le dernier element ajut�
		
		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre01);
		
		assertEquals(3, unGroupe.nbElements());

	}

	// Teste la methode ajouterDebut() Scenario 03

	@Test
	public void testAjouterDebutScenario03() {
		
		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);
		unGroupe.ajouterDebut(membre03);
		
		assertEquals(3, unGroupe.nbElements());
		
	// Scenario 03 : l'�l�ment est null
		
		membre04 = null; 
		
		unGroupe.ajouterDebut(membre04);

		Membre [] tab = parcourirGroupe (); 
		
		assertEquals(3, unGroupe.nbElements());
		
		//La position du premier �l�ment du groupe est occupee pour le dernier element ajut�
		
		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre01);

	}

	// Teste la methode ajouterFin() Scenario 01

	@Test
	public void testAjouterFinScenario01() {

		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		unGroupe.ajouterFin(membre03);
		
		assertEquals(3, unGroupe.nbElements());
		
		// Scenario 01 : l'�l�ment n'est pas null et il n'existe pas dans le groupe courant
		
		unGroupe.ajouterFin(membre04);

		Membre [] tab = parcourirGroupe (); 
		
		//La position du premier �l�ment du groupe est occupee pour le premier element ajut�
		
		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);
		assertEquals(tab[3], membre04);
		
		assertEquals(4, unGroupe.nbElements());

	}

	// Teste la methode ajouterFin() Scenario 02

	@Test
	public void testAjouterFinScenario02() {
		
		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		unGroupe.ajouterFin(membre03);
		
		assertEquals(3, unGroupe.nbElements());
		
	// Scenario 02 : l'�l�ment n'est pas null MAIS il existe deja dans le groupe courant
		
		unGroupe.ajouterFin(membre01); 
		
		Membre [] tab = parcourirGroupe (); 
		
		assertEquals(3, unGroupe.nbElements());
		
		//La position du premier �l�ment du groupe est occupee pour le premier element ajut�
		
		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);

	}

	// Teste la methode ajouterFin() Scenario 03

	@Test
	public void testAjouterFinScenario03() {
		
		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		unGroupe.ajouterFin(membre03);
		
		assertEquals(3, unGroupe.nbElements());
		
	// Scenario 03 : l'�l�ment est null
		
		membre04 = null; 
		
		unGroupe.ajouterFin(membre04);

		Membre [] tab = parcourirGroupe (); 
		
		assertEquals(3, unGroupe.nbElements());
		
		//La position du premier �l�ment du groupe est occupee pour le premier element ajut�
		
		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);

	}

	// Teste la methode estElement() Scenario 01

	@Test
	public void testEstElementScenario01() {

	// Scenario 01 : l'�l�ment existe d�ja dans le groupe courant

		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);

		assertTrue(unGroupe.estElement(membre02));

	}

	// Teste la methode estElement() Scenario 02

	@Test
	public void testEstElementScenario02() {

	// Scenario 02 : l'�l�ment n'existe pas dans le groupe courant

		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre03);

		assertFalse(unGroupe.estElement(membre02));

	}
	
	// Teste la methode ajouter () Scenario 01
	
	@Test
	public void testAjouterScenario01() {
		
	/*Scenario 01 : l'indice est dans le bon intervalle, 
					l'�l�ment n'est pas null et il n'existe pas dans le 
					groupe courant */
		
		unGroupe.ajouterDebut(membre01); 
		unGroupe.ajouterDebut(membre02); 
		unGroupe.ajouterDebut(membre03);	
		
		try {
			
			unGroupe.ajouter(0, membre04);
			
		} catch (PositionException e ) {
			
			assertEquals("PositionException", e.getClass().getSimpleName());
		}
		
		//L'�l�ment a �t� ajout� � l'indice (0) en �crasant l'ancien �l�ment du groupe courant
		
		Membre [] tab = parcourirGroupe (); 
		
		assertEquals(3, unGroupe.nbElements());
		
		assertEquals(tab[0], membre04);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre01);
		
	}
	
	// Teste la methode ajouter () Scenario 02
	
	@Test
	public void testAjouterScenario02() {
		
		/*Scenario 01 : l'indice N'EST PAS dans le bon intervalle, 
					l'�l�ment n'est pas null et il n'existe pas dans le 
					groupe courant */
		
		unGroupe.ajouterDebut(membre01); 
		unGroupe.ajouterDebut(membre02); 
		unGroupe.ajouterDebut(membre03); 
		
		assertEquals(3, unGroupe.nbElements());
		
		try {
			
			unGroupe.ajouter(6, membre04); //l'indice n'est pas dans le bon intervalle
			
		} catch (PositionException e ) {
			
			assertEquals("PositionException", e.getClass().getSimpleName());
		}
		
		//L'�l�ment n'a �t� pas ajout� � l'indice (0) et l'exception a �t� atrapper
		
		Membre [] tab = parcourirGroupe (); 
		
		assertEquals(3, unGroupe.nbElements());
		
		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre01);
		
	}
	
	// Teste la methode ajouter () Scenario 03
	
		@Test
		public void testAjouterScenario03() {
			
		/*Scenario 01 : l'indice est dans le bon intervalle, 
						l'�l�ment EST NULL et il n'existe pas dans le 
						groupe courant */
			
			unGroupe.ajouterDebut(membre01); 
			unGroupe.ajouterDebut(membre02); 
			unGroupe.ajouterDebut(membre03);
			
			assertEquals(3, unGroupe.nbElements());
			
			membre04 = null; 
			
			try {
				
				unGroupe.ajouter(0, membre04); // l'�l�ment est null
				
			} catch (PositionException e ) {
				
				assertEquals("PositionException", e.getClass().getSimpleName());
			}
			
			//L'�l�ment n'a �t� pas ajout� � l'indice (0) parce qu'il est null
			
			Membre [] tab = parcourirGroupe (); 
			
			assertEquals(3, unGroupe.nbElements());
			
			assertEquals(tab[0], membre03);
			assertEquals(tab[1], membre02);
			assertEquals(tab[2], membre01);
			
		}
		
		// Teste la methode ajouter () Scenario 04
		
		@Test
		public void testAjouterScenario04() {
			
			/*Scenario 01 : l'indice est dans le bon intervalle, 
						l'�l�ment est null et il EXISTE dans le 
						groupe courant */
			
			unGroupe.ajouterDebut(membre01); 
			unGroupe.ajouterDebut(membre02); 
			unGroupe.ajouterDebut(membre03);
			
			assertEquals(3, unGroupe.nbElements());
			
			
			try {
				
				unGroupe.ajouter(0, membre02); // l'�l�ment EXISTE dans le groupe courant
				
			} catch (PositionException e ) {
				
				assertEquals("PositionException", e.getClass().getSimpleName());
			}
			
			//L'�l�ment n'a �t� pas ajout� � l'indice (0) parce qu'il existe deja dans le groupe
			
			Membre [] tab = parcourirGroupe (); 
			
			assertEquals(3, unGroupe.nbElements());
			
			assertEquals(tab[0], membre03);
			assertEquals(tab[1], membre02);
			assertEquals(tab[2], membre01);
			
		}
		
		// Teste la methode ajouterDebut (groupe) Scenario 01
		
		@Test
		public void testAjouterDebutGroupeScenario01() {
			
			unGroupe.ajouterDebut(membre01); 
			unGroupe.ajouterDebut(membre02);
			
			assertEquals(2, unGroupe.nbElements());
			
			//Scenario 01: les �l�ments du groupe pass� en param�tre N'EXISTENT PAS dans le groupe
			
			GroupeTda<Membre> groupeAAjouter =  new GroupeImpl<Membre>();
			groupeAAjouter.ajouterDebut(membre03);
			groupeAAjouter.ajouterDebut(membre04);
			
			
			unGroupe.ajouterDebut(groupeAAjouter);
			
			assertEquals(4, unGroupe.nbElements());    
			
		}
		
		// Teste la methode ajouterDebut (groupe) Scenario 02
		
		@Test
		public void testAjouterDebutGroupeScenario02() {
			
			unGroupe.ajouterDebut(membre01); 
			unGroupe.ajouterDebut(membre02); 
			
			//Scenario 02: les �l�ments du groupe pass� en param�tre EXISTENT dans le groupe
			
			GroupeTda<Membre> groupeAAjouter =  new GroupeImpl<Membre>();
			groupeAAjouter.ajouterDebut(membre01);
			groupeAAjouter.ajouterDebut(membre02);
			
			List<Membre> groupeExisteDeja = new ArrayList<Membre>();
			
			groupeExisteDeja = unGroupe.ajouterDebut(groupeAAjouter);
			
			Iterator<Membre> itt = groupeExisteDeja.iterator();
			Membre [] tab = new Membre[groupeExisteDeja.size()]; 
			int i = 0; 
			
			while(itt.hasNext()){
				tab [i] = itt.next();
				i++; 
			}
			
			// ils EXISTENT deja 
			
			assertEquals(tab [0], membre02);
			assertEquals(tab [1], membre01);
			
			assertEquals(2, unGroupe.nbElements());    
			
		}
		// Teste la methode ajouterDebut (groupe) Scenario 03
		
		@Test
		public void testAjouterDebutGroupeScenario03() {
			
			unGroupe.ajouterDebut(membre01); 
			unGroupe.ajouterDebut(membre02); 
			
			//Scenario 03: le groupe pass� en param�tre EST null
			
			GroupeTda<Membre> groupeAAjouter =  new GroupeImpl<Membre>();
			
			groupeAAjouter = null; 
			
			List<Membre> groupeExisteDeja = new ArrayList<Membre>();
			
			groupeExisteDeja = unGroupe.ajouterDebut(groupeAAjouter);
			
			assertEquals(2, unGroupe.nbElements());  // aucunes �l�ments ont �t� ajoutes  
			assertEquals(0, groupeExisteDeja.size());    
			
		}
		
		// Teste la methode ajouterFin (groupe) Scenario 01
		
		@Test
		public void testAjouterFinGroupeScenario01() {
					
			unGroupe.ajouterFin(membre01); 
			unGroupe.ajouterFin(membre02); 
					
		//Scenario 01: les �l�ments du groupe pass� en param�tre N'EXISTENT PAS dans le groupe
					
			GroupeTda<Membre> groupeAAjouter =  new GroupeImpl<Membre>();
			groupeAAjouter.ajouterFin(membre03);
			groupeAAjouter.ajouterFin(membre04);
					
					
			unGroupe.ajouterFin(groupeAAjouter);
					
			assertEquals(4, unGroupe.nbElements());    
			assertEquals(membre01, unGroupe.iterateur().next());    
					
		}
				
		// Teste la methode ajouterFin (groupe) Scenario 02
				
		@Test
		public void testAjouterFinGroupeScenario02() {
					
			unGroupe.ajouterFin(membre01); 
			unGroupe.ajouterFin(membre02); 
					
		//Scenario 02: les �l�ments du groupe pass� en param�tre EXISTENT dans le groupe
					
			GroupeTda<Membre> groupeAAjouter =  new GroupeImpl<Membre>();
			groupeAAjouter.ajouterFin(membre01);
			groupeAAjouter.ajouterFin(membre02);
					
			List<Membre> groupeExisteDeja = new ArrayList<Membre>();
					
			groupeExisteDeja = unGroupe.ajouterFin(groupeAAjouter);
					
			Iterator<Membre> itt = groupeExisteDeja.iterator();
			Membre [] tab = new Membre[groupeExisteDeja.size()]; 
			int i = 0; 
					
			while(itt.hasNext()){
				tab [i] = itt.next();
				i++; 
			}
					
		// ils EXISTENT deja 
					
			assertEquals(tab [0], membre01);
			assertEquals(tab [1], membre02);
					
			assertEquals(2, unGroupe.nbElements());    
					
		}
		// Teste la methode ajouterFin (groupe) Scenario 03
				
		@Test
		public void testAjouterFinGroupeScenario03() {
					
			unGroupe.ajouterFin(membre01); 
			unGroupe.ajouterFin(membre02); 
					
		//Scenario 03: le groupe pass� en param�tre EST null
					
			GroupeTda<Membre> groupeAAjouter =  new GroupeImpl<Membre>();
					
			groupeAAjouter = null; 
					
			List<Membre> groupeExisteDeja = new ArrayList<Membre>();
					
			groupeExisteDeja = unGroupe.ajouterFin(groupeAAjouter);
					
			assertEquals(2, unGroupe.nbElements());  // aucunes �l�ments ont �t� ajoutes  
			assertEquals(0, groupeExisteDeja.size());    
					
		}
				
	// Teste la methode comparer(groupe) Scenario 01
	
	@Test
	public void testComparerGroupeScenario01 () {
		
		//Scenario 01: les �l�ments du groupe pass� en param�tre N'EXISTENT PAS dans le groupe
		
		unGroupe.ajouterDebut(membre01); 
		unGroupe.ajouterDebut(membre02); 
		
		
		GroupeTda<Membre> groupeAComparer =  new GroupeImpl<Membre>();
		
		groupeAComparer.ajouterDebut(membre03);
		groupeAComparer.ajouterDebut(membre04);
		
		
		List<Membre> groupeInexistent = new ArrayList<Membre>();
		
		groupeInexistent = unGroupe.comparer(groupeAComparer);
		
		Iterator<Membre> itt = groupeInexistent.iterator();
		Membre [] tab = new Membre[groupeInexistent.size()]; 
		int i = 0; 
		
		while(itt.hasNext()){
			tab [i] = itt.next();
			i++; 
		}
		
		// ils N'EXISTENT PAS dans le groupe
		
		assertEquals(tab [0], membre04);
		assertEquals(tab [1], membre03);
		  
		assertEquals(2, groupeInexistent.size());  
			
	}
	
	// Teste la methode comparer(groupe) Scenario 02
	
	@Test
	public void testComparerGroupeScenario02 () {
			
	//Scenario 02: les �l�ments du groupe pass� en param�tre EXISTENT dans le groupe
			
		unGroupe.ajouterDebut(membre01); 
		unGroupe.ajouterDebut(membre02); 
			
			
		GroupeTda<Membre> groupeAComparer =  new GroupeImpl<Membre>();
			
		groupeAComparer.ajouterDebut(membre01);
		groupeAComparer.ajouterDebut(membre02);
			
			
		List<Membre> groupeInexistent = new ArrayList<Membre>();
			
		groupeInexistent = unGroupe.comparer(groupeAComparer);
				  
		assertEquals(0, groupeInexistent.size());  
			
	}
		
	// Teste la methode suprimer(groupe)
	
	@Test
	public void testSuprimerGroupeScenario01() {
		
		unGroupe.ajouterDebut(membre01); 
		unGroupe.ajouterDebut(membre02); 
		
		//Scenario 01: les �l�ments du groupe pass� en param�tre N'EXISTENT PAS dans le groupe
		
		GroupeTda<Membre> groupeASuprimer =  new GroupeImpl<Membre>();
		groupeASuprimer.ajouterDebut(membre03);
		groupeASuprimer.ajouterDebut(membre04);
		
		
		List<Membre> groupeNonSuprimer = new ArrayList<Membre>();
		
		groupeNonSuprimer = unGroupe.supprimer(groupeASuprimer);
		
		Iterator<Membre> itt = groupeNonSuprimer.iterator();
		Membre [] tab = new Membre[groupeNonSuprimer.size()]; 
		int i = 0; 
		
		while(itt.hasNext()){
			tab [i] = itt.next();
			i++; 
		}
		
		assertEquals(tab [0], membre04);
		assertEquals(tab [1], membre03);
		
		assertEquals(2, unGroupe.nbElements());    
		
		
	}
	@Test
	public void testSuprimerGroupeScenario02() {
		
		unGroupe.ajouterDebut(membre01); 
		unGroupe.ajouterDebut(membre02); 
		unGroupe.ajouterDebut(membre03); 
		
		//Scenario 02: les �l�ments du groupe pass� en param�tre EXISTENT dans le groupe
		
		GroupeTda<Membre> groupeASuprimer =  new GroupeImpl<Membre>();
		groupeASuprimer.ajouterDebut(membre02);
		groupeASuprimer.ajouterDebut(membre03);
		
		
		List<Membre> groupeNonSuprimer = new ArrayList<Membre>();
		
		groupeNonSuprimer = unGroupe.supprimer(groupeASuprimer);
		
		Iterator<Membre> itt = groupeNonSuprimer.iterator();
		Membre [] tab = new Membre[groupeNonSuprimer.size()]; 
		int i = 0; 
		
		while(itt.hasNext()){
			tab [i] = itt.next();
			i++; 
		}
		
		assertEquals(1, unGroupe.nbElements());    
		assertEquals(0, groupeNonSuprimer.size()); //tous les �l�ments ont �t� supprim�s    
		
		assertFalse(unGroupe.estElement(membre02));
		assertFalse(unGroupe.estElement(membre03));
		
	}
	
	// Teste la methode remplacer () Scenario 03 
	
	@Test
	public void testSuprimerGroupeScenario03() {
		
		unGroupe.ajouterDebut(membre01); 
		unGroupe.ajouterDebut(membre02); 
		unGroupe.ajouterDebut(membre03); 
		
		//Scenario 03: le groupe pass� en param�tre EST null
		
		GroupeTda<Membre> groupeASuprimer =  new GroupeImpl<Membre>();
		
		groupeASuprimer = null; 
		
		List<Membre> groupeNonSuprimer = new ArrayList<Membre>();
		
		groupeNonSuprimer = unGroupe.supprimer(groupeASuprimer);
		
		Iterator<Membre> itt = groupeNonSuprimer.iterator();
		Membre [] tab = new Membre[groupeNonSuprimer.size()]; 
		int i = 0; 
		
		while(itt.hasNext()){
			tab [i] = itt.next();
			i++; 
		}
		
		assertEquals(3, unGroupe.nbElements()); // aucunes �l�ments ont �t� supprim�s    
		assertEquals(0, groupeNonSuprimer.size());     
		
	}
	
	
	// Teste la methode remplacer () Scenario 01 
	
	@Test
	
	public void testRemplacerScenario01() {
		
		/*Scenario 01 : L'�l�ment � remplacer existe dans le groupe, 
						le nouveau �l�ment n'est pas null;
						le groupe courant ne contient pas le nouveau �l�ment */
		
		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);
		unGroupe.ajouterDebut(membre03);
		
		unGroupe.remplacer(membre02, membre04); 
		
		Membre [] taba = parcourirGroupe (); 
		
		assertEquals(3, unGroupe.nbElements());
		
		assertEquals(taba[0], membre03);
		assertEquals(taba[1], membre04);
		assertEquals(taba[2], membre01);
	}
	
	// Teste la methode remplacer () Scenario 02 
	
	@Test
	
	public void testRemplacerScenario02() {
		
		/*Scenario 02 : L'�l�ment � remplacer existe dans le groupe, 
						le nouveau �l�ment n'est pas null,
						le groupe courant CONTIENT le nouveau �l�ment*/
		
		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);
		unGroupe.ajouterDebut(membre03);
		
		//aucun remplacement parse que le nouveau �l�ment existe deja dans le groupe
		
		assertFalse(unGroupe.remplacer(membre02, membre01));// return false
	}
	
	// Teste la methode remplacer () Scenario 03 
	
	@Test
	
	public void testRemplacerScenario03() {
		
		/*Scenario 03 : L'�l�ment � remplacer existe dans le groupe, 
						le nouveau �l�ment EST null,
						le groupe courant ne contient pas le nouveau �l�ment*/
		
		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);
		unGroupe.ajouterDebut(membre03);
		
		//aucun remplacement parse que le nouveau �l�ment est null
		
		membre04 = null;
		
		assertFalse(unGroupe.remplacer(membre01, membre04));// return false
	}
	
	// Teste la methode remplacer () Scenario 04 
	
	@Test
	
	public void testRemplacerScenario04() {
		
		/*Scenario 04 : L'�l�ment � remplacer N'EXISTE PAS dans le groupe, 
						le nouveau �l�ment n'est pas null,,
						le groupe courant ne contient pas le nouveau �l�ment*/
		
		unGroupe.ajouterDebut(membre01);
		unGroupe.ajouterDebut(membre02);
		
		//aucun remplacement parse que l'�l�ment � remplacer n'existe pas dans le groupe
		
		assertFalse(unGroupe.remplacer(membre04, membre03));// return false
	}
		
	// Teste la methode nbElements () 
	
	@Test
	public void testNbElements() {
		
       assertEquals(0, unGroupe.nbElements()); // nombre d'�l�ments : 0
		
       unGroupe.ajouterDebut(membre01);
       unGroupe.ajouterDebut(membre02);
		
       assertEquals(2, unGroupe.nbElements()); // nombre d'�l�ments : 2
	}
	
	// Teste la methode suprimer() Scenario 01
	
	@Test
	public void testSuprimerScenario01() {
		
		// Scenario 01 : l'�l�ment existe dans le groupe courant
		
		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		unGroupe.ajouterFin(membre03);
		
		assertEquals(3, unGroupe.nbElements()); // nombre d'�l�ments AVANT suprimer(): 3
		
		//return true si l'�l�ment est supprim�
		
		assertTrue(unGroupe.supprimer(membre02));
		
		// l'�l�ment (membre02) a �t� supprim� dans le groupe courant
		
       assertEquals(2, unGroupe.nbElements()); // nombre d'�l�ments APRES suprimer(): 2
	}
	
	// Teste la methode suprimer() Scenario 02
	
	@Test
	public void testSuprimerScenario02() {
		
		// Scenario 02 : l'�l�ment n'existe pas dans le groupe courant
		
		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		unGroupe.ajouterFin(membre03);
		
		assertEquals(3, unGroupe.nbElements()); // nombre d'�l�ments AVANT suprimer(): 3
		
		//return false si l'�l�ment n'est pas supprim�
		
		assertFalse(unGroupe.supprimer(membre04));
		
		// aucun �l�ment est supprim� dans le groupe courant
		
		assertEquals(3, unGroupe.nbElements()); // nombre d'�l�ments APRES suprimer(): 3
	}
	
	
	// Teste la methode estVide () 
	
	@Test
	public void testEstVide() {
				
		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		
		//return false si le groupe courant n'est pas vide
		
		assertFalse(unGroupe.estVide());
		
	}
	
	// Teste la methode element () 
	@Test
	
	public void testElement (){
		
		unGroupe.elements().put(0, membre01); 
		unGroupe.elements().put(1, membre02); 
		
		assertEquals(null, unGroupe.elements().get(membre02)); 
	}
	
	// Teste la methode vider () 
	
	@Test
	public void testVider() {
				
		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		
		assertFalse(unGroupe.estVide());// le groupe n'est pas vide
		
		unGroupe.vider();
		
		assertTrue(unGroupe.estVide());// le groupe est vide APRES vider()
	}
	
	// Teste la methode iterateur ()
	
	@Test
	public void testIterateur() {
		
		unGroupe.ajouterFin(membre01);
		unGroupe.ajouterFin(membre02);
		
		// le groupe a un prochain �l�ment
		
		assertEquals(membre01,unGroupe.iterateur().next()); // teste .next()
		
		unGroupe.vider();
		
		// le groupe n'a pas un prochain �l�ment
		
		assertFalse(unGroupe.iterateur().hasNext()); // teste .hasNext()
	}
	
}
