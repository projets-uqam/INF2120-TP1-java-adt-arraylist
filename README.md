ADT using ArrayList
===================================================
TP1 - A2015 - INF2120 - Programmation II


Summary
---------
* Implementing interface
* Manipulating Abstract Data Type «ADT»
* ArrayList Implementation
* Writing and running JUnit tests


Technologies used:
------------------
* Java 7


Reference
----------
* INF2120 | A2015 | Professeur: Ismael Doukoure


License
--------
This project is licensed under the Apache License - see [here](https://www.gnu.org/licenses/gpl-3.0.en.html) for details